\contentsline {section}{\numberline {I}Introduction}{1}
\contentsline {section}{\numberline {II}Collaborative Problem Solving}{2}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {II-A}}CPS Assessments}{2}
\contentsline {section}{\numberline {III}Computational Psychometrics}{3}
\contentsline {section}{\numberline {IV}CPS Game: Circuit Runner}{4}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {IV-A}}Extract}{5}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {IV-B}}Transform}{5}
\contentsline {section}{\numberline {V}Cluster Analysis}{5}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {V-A}}Research Question}{5}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {V-B}}Related Work}{5}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {V-C}}K-Means Methodology}{5}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {V-C}1}Encode/Translate}{5}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {V-C}2}Score}{6}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {V-C}3}Cluster}{6}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {V-C}4}K Exploration}{6}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {V-D}}K-Means/K-NN Results}{7}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {V-D}1}Cluster Characteristics}{7}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {V-D}2}K-NN Query by Game Id}{8}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {V-E}}Mixture Model Methodology}{9}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {V-F}}Mixture Model Results}{9}
\contentsline {section}{\numberline {VI}Bayesian Analysis}{10}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {VI-A}}Research Question}{10}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {VI-B}}Methodology}{10}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {VI-C}}Select}{11}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {VI-D}}Compute}{11}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {VI-D}1}Bayes' Theorem}{11}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {VI-D}2}Response to Skill}{12}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {VI-D}3}Conditional Probability Tables}{12}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {VI-D}4}Evidence Tracing}{12}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {VI-E}}Plot}{13}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {VI-F}}Extend}{13}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {VI-G}}Results}{13}
\contentsline {section}{References}{14}
\contentsline {section}{\numberline {VII}Appendix 1: Tableau Score/Cluster Distribution}{16}
\contentsline {section}{\numberline {VIII}Appendix 2: Tableau Distribution EN/MU/FI}{17}
