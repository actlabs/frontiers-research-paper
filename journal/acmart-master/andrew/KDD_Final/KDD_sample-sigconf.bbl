%%% -*-BibTeX-*-
%%% Do NOT edit. File created by BibTeX with style
%%% ACM-Reference-Format-Journals [18-Jan-2012].

\begin{thebibliography}{00}

%%% ====================================================================
%%% NOTE TO THE USER: you can override these defaults by providing
%%% customized versions of any of these macros before the \bibliography
%%% command.  Each of them MUST provide its own final punctuation,
%%% except for \shownote{}, \showDOI{}, and \showURL{}.  The latter two
%%% do not use final punctuation, in order to avoid confusing it with
%%% the Web address.
%%%
%%% To suppress output of a particular field, define its macro to expand
%%% to an empty string, or better, \unskip, like this:
%%%
%%% \newcommand{\showDOI}[1]{\unskip}   % LaTeX syntax
%%%
%%% \def \showDOI #1{\unskip}           % plain TeX syntax
%%%
%%% ====================================================================

\ifx \showCODEN    \undefined \def \showCODEN     #1{\unskip}     \fi
\ifx \showDOI      \undefined \def \showDOI       #1{#1}\fi
\ifx \showISBNx    \undefined \def \showISBNx     #1{\unskip}     \fi
\ifx \showISBNxiii \undefined \def \showISBNxiii  #1{\unskip}     \fi
\ifx \showISSN     \undefined \def \showISSN      #1{\unskip}     \fi
\ifx \showLCCN     \undefined \def \showLCCN      #1{\unskip}     \fi
\ifx \shownote     \undefined \def \shownote      #1{#1}          \fi
\ifx \showarticletitle \undefined \def \showarticletitle #1{#1}   \fi
\ifx \showURL      \undefined \def \showURL       {\relax}        \fi
% The following commands are used for tagged output and should be
% invisible to TeX
\providecommand\bibfield[2]{#2}
\providecommand\bibinfo[2]{#2}
\providecommand\natexlab[1]{#1}
\providecommand\showeprint[2][]{arXiv:#2}

\bibitem[\protect\citeauthoryear{Arya, Mount, Netanyahu, Silverman, and
  Wu}{Arya et~al\mbox{.}}{1998}]%
        {arya1998optimal}
\bibfield{author}{\bibinfo{person}{Sunil Arya}, \bibinfo{person}{David~M
  Mount}, \bibinfo{person}{Nathan~S Netanyahu}, \bibinfo{person}{Ruth
  Silverman}, {and} \bibinfo{person}{Angela~Y Wu}.}
  \bibinfo{year}{1998}\natexlab{}.
\newblock \showarticletitle{An optimal algorithm for approximate nearest
  neighbor searching fixed dimensions}.
\newblock \bibinfo{journal}{{\em Journal of the ACM (JACM)\/}}
  \bibinfo{volume}{45}, \bibinfo{number}{6} (\bibinfo{year}{1998}),
  \bibinfo{pages}{891--923}.
\newblock


\bibitem[\protect\citeauthoryear{Ashton, Lee, Perugini, Szarota, De~Vries,
  Di~Blas, Boies, and De~Raad}{Ashton et~al\mbox{.}}{2004}]%
        {ashton2004six}
\bibfield{author}{\bibinfo{person}{Michael~C Ashton}, \bibinfo{person}{Kibeom
  Lee}, \bibinfo{person}{Marco Perugini}, \bibinfo{person}{Piotr Szarota},
  \bibinfo{person}{Reinout~E De~Vries}, \bibinfo{person}{Lisa Di~Blas},
  \bibinfo{person}{Kathleen Boies}, {and} \bibinfo{person}{Boele De~Raad}.}
  \bibinfo{year}{2004}\natexlab{}.
\newblock \showarticletitle{A six-factor structure of personality-descriptive
  adjectives: solutions from psycholexical studies in seven languages.}
\newblock \bibinfo{journal}{{\em Journal of personality and social
  psychology\/}} \bibinfo{volume}{86}, \bibinfo{number}{2}
  (\bibinfo{year}{2004}), \bibinfo{pages}{356}.
\newblock


\bibitem[\protect\citeauthoryear{Bauckhage, Drachen, and Sifa}{Bauckhage
  et~al\mbox{.}}{2015}]%
        {bauckhage2015clustering}
\bibfield{author}{\bibinfo{person}{Christian Bauckhage},
  \bibinfo{person}{Anders Drachen}, {and} \bibinfo{person}{Rafet Sifa}.}
  \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{Clustering game behavior data}.
\newblock \bibinfo{journal}{{\em IEEE Transactions on Computational
  Intelligence and AI in Games\/}} \bibinfo{volume}{7}, \bibinfo{number}{3}
  (\bibinfo{year}{2015}), \bibinfo{pages}{266--278}.
\newblock


\bibitem[\protect\citeauthoryear{Bayes and Price}{Bayes and Price}{1763}]%
        {mr1763essay}
\bibfield{author}{\bibinfo{person}{Mr. Bayes} {and} \bibinfo{person}{Mr.
  Price}.} \bibinfo{year}{1763}\natexlab{}.
\newblock \showarticletitle{An essay towards solving a problem in the doctrine
  of chances. by the late rev. mr. bayes, frs communicated by mr. price, in a
  letter to john canton, amfrs}.
\newblock \bibinfo{journal}{{\em Philosophical Transactions (1683-1775)\/}}
  (\bibinfo{year}{1763}), \bibinfo{pages}{370--418}.
\newblock


\bibitem[\protect\citeauthoryear{Bazaldua, Khan, von Davier, Hao, Liu, and
  Wang}{Bazaldua et~al\mbox{.}}{2015}]%
        {bazaldua2015convergence}
\bibfield{author}{\bibinfo{person}{DL Bazaldua}, \bibinfo{person}{S Khan},
  \bibinfo{person}{A.~A. von Davier}, \bibinfo{person}{J Hao},
  \bibinfo{person}{L Liu}, {and} \bibinfo{person}{Z Wang}.}
  \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{On convergence of cognitive and non-cognitive
  behavior in collaborative activity}. In \bibinfo{booktitle}{{\em The 8th
  International Conference on Educational Data Mining (EDM 2015)}}.
\newblock


\bibitem[\protect\citeauthoryear{Bergner, Andrews, Zhu, and Kitchen}{Bergner
  et~al\mbox{.}}{July}]%
        {bergnerjuly2015}
\bibfield{author}{\bibinfo{person}{Y. Bergner}, \bibinfo{person}{J.~J.
  Andrews}, \bibinfo{person}{M. Zhu}, {and} \bibinfo{person}{C. Kitchen}.}
  \bibinfo{year}{2015, July}\natexlab{}.
\newblock \showarticletitle{Agent-based modeling of collaborative problem
  solving.}
\newblock \bibinfo{journal}{{\em Paper presented at the 10th Annual INGRoup
  Conference, Pittsburgh, PA.\/}} (\bibinfo{year}{2015, July}).
\newblock


\bibitem[\protect\citeauthoryear{Camara, O'Connor, Mattern, and Hanson}{Camara
  et~al\mbox{.}}{2015}]%
        {camara2015beyond}
\bibfield{author}{\bibinfo{person}{Wayne Camara}, \bibinfo{person}{Ryan
  O'Connor}, \bibinfo{person}{Krista Mattern}, {and} \bibinfo{person}{Mary~Ann
  Hanson}.} \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{Beyond Academics: A Holistic Framework for
  Enhancing Education and Workplace Success. ACT Research Report Series. 2015
  (4).}
\newblock \bibinfo{journal}{{\em ACT, Inc.\/}} (\bibinfo{year}{2015}).
\newblock


\bibitem[\protect\citeauthoryear{Canossa}{Canossa}{2013}]%
        {canossa2013meaning}
\bibfield{author}{\bibinfo{person}{Alessandro Canossa}.}
  \bibinfo{year}{2013}\natexlab{}.
\newblock \showarticletitle{Meaning in gameplay: Filtering variables, defining
  metrics, extracting features and creating models for gameplay analysis}.
\newblock In \bibinfo{booktitle}{{\em Game Analytics}}.
  \bibinfo{publisher}{Springer}, \bibinfo{pages}{255--283}.
\newblock


\bibitem[\protect\citeauthoryear{Corbett and Anderson}{Corbett and
  Anderson}{1994}]%
        {corbett1994knowledge}
\bibfield{author}{\bibinfo{person}{Albert~T Corbett} {and}
  \bibinfo{person}{John~R Anderson}.} \bibinfo{year}{1994}\natexlab{}.
\newblock \showarticletitle{Knowledge tracing: Modeling the acquisition of
  procedural knowledge}.
\newblock \bibinfo{journal}{{\em User modeling and user-adapted interaction\/}}
  \bibinfo{volume}{4}, \bibinfo{number}{4} (\bibinfo{year}{1994}),
  \bibinfo{pages}{253--278}.
\newblock


\bibitem[\protect\citeauthoryear{Davey, Ferrara, Shavelson, Holland, Webb, and
  Wise}{Davey et~al\mbox{.}}{2015}]%
        {davey2015psychometric}
\bibfield{author}{\bibinfo{person}{Tim Davey}, \bibinfo{person}{Steve Ferrara},
  \bibinfo{person}{R Shavelson}, \bibinfo{person}{P Holland},
  \bibinfo{person}{N Webb}, {and} \bibinfo{person}{L Wise}.}
  \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{Psychometric considerations for the next generation
  of performance assessment}.
\newblock \bibinfo{journal}{{\em Washington, DC: Center for K-12 Assessment \&
  Performance Management, Educational Testing Service\/}}
  (\bibinfo{year}{2015}).
\newblock


\bibitem[\protect\citeauthoryear{Desmarais and Baker}{Desmarais and
  Baker}{2012}]%
        {desmarais2012review}
\bibfield{author}{\bibinfo{person}{Michel~C Desmarais} {and}
  \bibinfo{person}{Ryan~S Baker}.} \bibinfo{year}{2012}\natexlab{}.
\newblock \showarticletitle{A review of recent advances in learner and skill
  modeling in intelligent learning environments}.
\newblock \bibinfo{journal}{{\em User Modeling and User-Adapted Interaction\/}}
  \bibinfo{volume}{22}, \bibinfo{number}{1-2} (\bibinfo{year}{2012}),
  \bibinfo{pages}{9--38}.
\newblock


\bibitem[\protect\citeauthoryear{Gow, Baumgarten, Cairns, Colton, and
  Miller}{Gow et~al\mbox{.}}{2012}]%
        {gow2012unsupervised}
\bibfield{author}{\bibinfo{person}{Jeremy Gow}, \bibinfo{person}{Robin
  Baumgarten}, \bibinfo{person}{Paul Cairns}, \bibinfo{person}{Simon Colton},
  {and} \bibinfo{person}{Paul Miller}.} \bibinfo{year}{2012}\natexlab{}.
\newblock \showarticletitle{Unsupervised modeling of player style with LDA}.
\newblock \bibinfo{journal}{{\em IEEE Transactions on Computational
  Intelligence and AI in Games\/}} \bibinfo{volume}{4}, \bibinfo{number}{3}
  (\bibinfo{year}{2012}), \bibinfo{pages}{152--166}.
\newblock


\bibitem[\protect\citeauthoryear{Griffin, McGaw, and Care}{Griffin
  et~al\mbox{.}}{2012}]%
        {griffin2012assessment}
\bibfield{author}{\bibinfo{person}{Patrick Griffin}, \bibinfo{person}{Barry
  McGaw}, {and} \bibinfo{person}{Esther Care}.}
  \bibinfo{year}{2012}\natexlab{}.
\newblock \bibinfo{booktitle}{{\em Assessment and teaching of 21st century
  skills}}.
\newblock \bibinfo{publisher}{Springer}.
\newblock


\bibitem[\protect\citeauthoryear{Hao, Liu, von Davier, Kyllonen, and
  Kitchen}{Hao et~al\mbox{.}}{2016}]%
        {haocollaborative}
\bibfield{author}{\bibinfo{person}{Jiangang Hao}, \bibinfo{person}{Lei Liu},
  \bibinfo{person}{Alina~A von Davier}, \bibinfo{person}{Patrick Kyllonen},
  {and} \bibinfo{person}{Christopher Kitchen}.}
  \bibinfo{year}{2016}\natexlab{}.
\newblock \showarticletitle{Collaborative Problem Solving Skills versus
  Collaboration Outcomes: Findings from Statistical Analysis and Data Mining}.
  In \bibinfo{booktitle}{{\em Proceedings of EDM}}.
\newblock


\bibitem[\protect\citeauthoryear{He, von Davier, Greiff, Steinhauer, and
  Borysewicz}{He et~al\mbox{.}}{2017}]%
        {he2017collaborative}
\bibfield{author}{\bibinfo{person}{Qiwei He}, \bibinfo{person}{Matthias von
  Davier}, \bibinfo{person}{Samuel Greiff}, \bibinfo{person}{Eric~W
  Steinhauer}, {and} \bibinfo{person}{Paul~B Borysewicz}.}
  \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{Collaborative problem solving measures in the
  programme for international student assessment (PISA)}.
\newblock In \bibinfo{booktitle}{{\em Innovative Assessment of Collaboration}}.
  \bibinfo{publisher}{Springer}, \bibinfo{pages}{95--111}.
\newblock


\bibitem[\protect\citeauthoryear{Kerr}{Kerr}{2015}]%
        {kerr2015using}
\bibfield{author}{\bibinfo{person}{Deirdre Kerr}.}
  \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{Using data mining results to improve educational
  video game design}.
\newblock \bibinfo{journal}{{\em JEDM-Journal of Educational Data Mining\/}}
  \bibinfo{volume}{7}, \bibinfo{number}{3} (\bibinfo{year}{2015}),
  \bibinfo{pages}{1--17}.
\newblock


\bibitem[\protect\citeauthoryear{Kerr and Chung}{Kerr and Chung}{2012}]%
        {kerr2012identifying}
\bibfield{author}{\bibinfo{person}{Deirdre Kerr} {and}
  \bibinfo{person}{Gregory~KWK Chung}.} \bibinfo{year}{2012}\natexlab{}.
\newblock \showarticletitle{Identifying key features of student performance in
  educational video games and simulations through cluster analysis}.
\newblock \bibinfo{journal}{{\em JEDM-Journal of Educational Data Mining\/}}
  \bibinfo{volume}{4}, \bibinfo{number}{1} (\bibinfo{year}{2012}),
  \bibinfo{pages}{144--182}.
\newblock


\bibitem[\protect\citeauthoryear{Kerr, Chung, and Iseli}{Kerr
  et~al\mbox{.}}{2011}]%
        {kerr2011feasibility}
\bibfield{author}{\bibinfo{person}{Deirdre Kerr}, \bibinfo{person}{Gregory~KWK
  Chung}, {and} \bibinfo{person}{Markus~R Iseli}.}
  \bibinfo{year}{2011}\natexlab{}.
\newblock \showarticletitle{The Feasibility of Using Cluster Analysis to
  Examine Log Data from Educational Video Games. CRESST Report 790.}
\newblock \bibinfo{journal}{{\em National Center for Research on Evaluation,
  Standards, and Student Testing (CRESST)\/}} (\bibinfo{year}{2011}).
\newblock


\bibitem[\protect\citeauthoryear{Khan}{Khan}{2015}]%
        {khan2015}
\bibfield{author}{\bibinfo{person}{S. Khan}.}
  \bibinfo{year}{September,2015}\natexlab{}.
\newblock \bibinfo{title}{Multimodal behavioral analytics for intelligent
  training and assessment systems. Presentation at the Rutgers University.}
\newblock   (\bibinfo{year}{September,2015}).
\newblock
\showURL{%
\url{http://www.ece.rutgers.edu/node/2111}}


\bibitem[\protect\citeauthoryear{Khan, Cheng, and Kumar}{Khan
  et~al\mbox{.}}{2013}]%
        {khan2013hierarchical}
\bibfield{author}{\bibinfo{person}{Saad Khan}, \bibinfo{person}{Hui Cheng},
  {and} \bibinfo{person}{Rakesh Kumar}.} \bibinfo{year}{2013}\natexlab{}.
\newblock \showarticletitle{A hierarchical behavior analysis approach for
  automated trainee performance evaluation in training ranges}. In
  \bibinfo{booktitle}{{\em International Conference on Augmented Cognition}}.
  Springer, \bibinfo{pages}{60--69}.
\newblock


\bibitem[\protect\citeauthoryear{LaMar}{LaMar}{2014}]%
        {lamar2014models}
\bibfield{author}{\bibinfo{person}{Michelle~Marie LaMar}.}
  \bibinfo{year}{2014}\natexlab{}.
\newblock {\em \bibinfo{title}{Models for Understanding Student Thinking Using
  Data from Complex Computerized Science Tasks}}.
\newblock \bibinfo{thesistype}{Ph.D. Dissertation}. \bibinfo{school}{University
  of California, Berkeley}.
\newblock


\bibitem[\protect\citeauthoryear{Levy}{Levy}{2014}]%
        {levy2014dynamic}
\bibfield{author}{\bibinfo{person}{Roy Levy}.} \bibinfo{year}{2014}\natexlab{}.
\newblock \showarticletitle{Dynamic Bayesian Network Modeling of Game Based
  Diagnostic Assessments. CRESST Report 837.}
\newblock \bibinfo{journal}{{\em National Center for Research on Evaluation,
  Standards, and Student Testing (CRESST)\/}} (\bibinfo{year}{2014}).
\newblock


\bibitem[\protect\citeauthoryear{Li, Munoz-Avila, Ke, Symborski, and Alonso}{Li
  et~al\mbox{.}}{2013}]%
        {li2013discovery}
\bibfield{author}{\bibinfo{person}{Hua Li}, \bibinfo{person}{Hector
  Munoz-Avila}, \bibinfo{person}{Lei Ke}, \bibinfo{person}{Carl Symborski},
  {and} \bibinfo{person}{Rafael Alonso}.} \bibinfo{year}{2013}\natexlab{}.
\newblock \showarticletitle{Discovery of Player Strategies in a Serious Game}.
  In \bibinfo{booktitle}{{\em First AAAI Conference on Human Computation and
  Crowdsourcing}}.
\newblock


\bibitem[\protect\citeauthoryear{McLachlan and Basford}{McLachlan and
  Basford}{1988}]%
        {mclachlan1988mixture}
\bibfield{author}{\bibinfo{person}{Geoffrey~J McLachlan} {and}
  \bibinfo{person}{Kaye~E Basford}.} \bibinfo{year}{1988}\natexlab{}.
\newblock \showarticletitle{Mixture models. Inference and applications to
  clustering}.
\newblock \bibinfo{journal}{{\em Statistics: Textbooks and Monographs, New
  York: Dekker, 1988\/}}  \bibinfo{volume}{1} (\bibinfo{year}{1988}).
\newblock


\bibitem[\protect\citeauthoryear{Mislevy, Oranje, Bauer, von Davier, Hao,
  Corrigan, Hoffman, DiCerbo, and John}{Mislevy et~al\mbox{.}}{2014}]%
        {mislevy2014psychometric}
\bibfield{author}{\bibinfo{person}{Robert~J Mislevy}, \bibinfo{person}{Andreas
  Oranje}, \bibinfo{person}{Malcolm~I Bauer}, \bibinfo{person}{Alina~A. von
  Davier}, \bibinfo{person}{Jiangang Hao}, \bibinfo{person}{Seth Corrigan},
  \bibinfo{person}{Erin Hoffman}, \bibinfo{person}{Kristen DiCerbo}, {and}
  \bibinfo{person}{Michael John}.} \bibinfo{year}{2014}\natexlab{}.
\newblock \showarticletitle{Psychometric considerations in game-based
  assessment}.
\newblock \bibinfo{journal}{{\em GlassLab Report\/}} (\bibinfo{year}{2014}).
\newblock


\bibitem[\protect\citeauthoryear{Mislevy, Steinberg, and Almond}{Mislevy
  et~al\mbox{.}}{2003}]%
        {mislevy2003focus}
\bibfield{author}{\bibinfo{person}{Robert~J Mislevy}, \bibinfo{person}{Linda~S
  Steinberg}, {and} \bibinfo{person}{Russell~G Almond}.}
  \bibinfo{year}{2003}\natexlab{}.
\newblock \showarticletitle{Focus article: On the structure of educational
  assessments}.
\newblock \bibinfo{journal}{{\em Measurement: Interdisciplinary research and
  perspectives\/}} \bibinfo{volume}{1}, \bibinfo{number}{1}
  (\bibinfo{year}{2003}), \bibinfo{pages}{3--62}.
\newblock


\bibitem[\protect\citeauthoryear{Mislevy, Steinberg, Almond, and Lukas}{Mislevy
  et~al\mbox{.}}{2006}]%
        {mislevy2006concepts}
\bibfield{author}{\bibinfo{person}{Robert~J Mislevy}, \bibinfo{person}{Linda~S
  Steinberg}, \bibinfo{person}{Russell~G Almond}, {and}
  \bibinfo{person}{Janice~F Lukas}.} \bibinfo{year}{2006}\natexlab{}.
\newblock \showarticletitle{Concepts, terminology, and basic models of
  evidence-centered design}.
\newblock \bibinfo{journal}{{\em Automated scoring of complex tasks in
  computer-based testing\/}} (\bibinfo{year}{2006}), \bibinfo{pages}{15--47}.
\newblock


\bibitem[\protect\citeauthoryear{NCES}{NCES}{2017}]%
        {naep2017}
\bibfield{author}{\bibinfo{person}{NCES}.} \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{Collaborative Problem Solving: Considerations for
  the National Assessment of Educational Progress.}
\newblock  (\bibinfo{year}{2017}).
\newblock
\showURL{%
\url{https://nces.ed.gov/nationsreportcard/pdf/researchcenter/collaborative_problem_solving.pdf}}


\bibitem[\protect\citeauthoryear{Oliveri, Lawless, and Molloy}{Oliveri
  et~al\mbox{.}}{2017}]%
        {oliveri2017literature}
\bibfield{author}{\bibinfo{person}{Mar{\'\i}a~Elena Oliveri},
  \bibinfo{person}{Ren{\'e} Lawless}, {and} \bibinfo{person}{Hillary Molloy}.}
  \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{A Literature Review on Collaborative Problem
  Solving for College and Workforce Readiness}.
\newblock \bibinfo{journal}{{\em ETS Research Report Series\/}}
  (\bibinfo{year}{2017}).
\newblock


\bibitem[\protect\citeauthoryear{Orkin and Roy}{Orkin and Roy}{2011}]%
        {orkin2011semi}
\bibfield{author}{\bibinfo{person}{Jeff Orkin} {and} \bibinfo{person}{Deb
  Roy}.} \bibinfo{year}{2011}\natexlab{}.
\newblock \showarticletitle{Semi-automated dialogue act classification for
  situated social agents in games}.
\newblock In \bibinfo{booktitle}{{\em Agents for games and simulations II}}.
  \bibinfo{publisher}{Springer}, \bibinfo{pages}{148--162}.
\newblock


\bibitem[\protect\citeauthoryear{Paulhus, Vazire, Robins, Fraley, and
  Krueger}{Paulhus et~al\mbox{.}}{2007}]%
        {paulhus2007self}
\bibfield{author}{\bibinfo{person}{Delroy~L Paulhus}, \bibinfo{person}{Simine
  Vazire}, \bibinfo{person}{Richard~W Robins}, \bibinfo{person}{RC Fraley},
  {and} \bibinfo{person}{Robert~F Krueger}.} \bibinfo{year}{2007}\natexlab{}.
\newblock \showarticletitle{The self-report method}.
\newblock \bibinfo{journal}{{\em Handbook of research methods in personality
  psychology\/}}  \bibinfo{volume}{1} (\bibinfo{year}{2007}),
  \bibinfo{pages}{224--239}.
\newblock


\bibitem[\protect\citeauthoryear{{PISA, OECD}}{{PISA, OECD}}{2013}]%
        {pisa2013results}
\bibfield{author}{\bibinfo{person}{{PISA, OECD}}.}
  \bibinfo{year}{2013}\natexlab{}.
\newblock \bibinfo{title}{Results: Excellence Through Equity: Giving Every
  Student the Chance to Succeed (volume II)}.
\newblock   (\bibinfo{year}{2013}).
\newblock


\bibitem[\protect\citeauthoryear{Robins and John}{Robins and John}{1997}]%
        {robins1997quest}
\bibfield{author}{\bibinfo{person}{Richard~W Robins} {and}
  \bibinfo{person}{Oliver~P John}.} \bibinfo{year}{1997}\natexlab{}.
\newblock \showarticletitle{The quest for self-insight: Theory and research on
  accuracy and bias in self-perception.}
\newblock  (\bibinfo{year}{1997}).
\newblock


\bibitem[\protect\citeauthoryear{Romero, Gonz{\'a}lez, Ventura, Del~Jes{\'u}s,
  and Herrera}{Romero et~al\mbox{.}}{2009}]%
        {romero2009evolutionary}
\bibfield{author}{\bibinfo{person}{Crist{\'o}bal Romero},
  \bibinfo{person}{Pedro Gonz{\'a}lez}, \bibinfo{person}{Sebasti{\'a}n
  Ventura}, \bibinfo{person}{Mar{\'\i}a~Jos{\'e} Del~Jes{\'u}s}, {and}
  \bibinfo{person}{Francisco Herrera}.} \bibinfo{year}{2009}\natexlab{}.
\newblock \showarticletitle{Evolutionary algorithms for subgroup discovery in
  e-learning: A practical application using Moodle data}.
\newblock \bibinfo{journal}{{\em Expert Systems with Applications\/}}
  \bibinfo{volume}{36}, \bibinfo{number}{2} (\bibinfo{year}{2009}),
  \bibinfo{pages}{1632--1644}.
\newblock


\bibitem[\protect\citeauthoryear{S{\'a}nchez and Olivares}{S{\'a}nchez and
  Olivares}{2011}]%
        {sanchez2011problem}
\bibfield{author}{\bibinfo{person}{Jaime S{\'a}nchez} {and}
  \bibinfo{person}{Ruby Olivares}.} \bibinfo{year}{2011}\natexlab{}.
\newblock \showarticletitle{Problem solving and collaboration using mobile
  serious games}.
\newblock  (\bibinfo{year}{2011}).
\newblock


\bibitem[\protect\citeauthoryear{Shute, Hansen, and Almond}{Shute
  et~al\mbox{.}}{2008a}]%
        {shute2008you}
\bibfield{author}{\bibinfo{person}{Valerie~J Shute}, \bibinfo{person}{Eric~G
  Hansen}, {and} \bibinfo{person}{Russell~G Almond}.}
  \bibinfo{year}{2008}\natexlab{a}.
\newblock \showarticletitle{You can't fatten A hog by weighing It--Or can you?
  evaluating an assessment for learning system called ACED}.
\newblock \bibinfo{journal}{{\em International Journal of Artificial
  Intelligence in Education\/}} \bibinfo{volume}{18}, \bibinfo{number}{4}
  (\bibinfo{year}{2008}), \bibinfo{pages}{289--316}.
\newblock


\bibitem[\protect\citeauthoryear{Shute, Ventura, Bauer, and
  Zapata-Rivera}{Shute et~al\mbox{.}}{2008b}]%
        {shute2008monitoring}
\bibfield{author}{\bibinfo{person}{Valerie~J Shute}, \bibinfo{person}{Matthew
  Ventura}, \bibinfo{person}{Malcolm Bauer}, {and} \bibinfo{person}{Diego
  Zapata-Rivera}.} \bibinfo{year}{2008}\natexlab{b}.
\newblock \showarticletitle{Monitoring and fostering learning through games and
  embedded assessments}.
\newblock \bibinfo{journal}{{\em ETS Research Report Series\/}}
  \bibinfo{volume}{2008}, \bibinfo{number}{2} (\bibinfo{year}{2008}).
\newblock


\bibitem[\protect\citeauthoryear{Smith}{Smith}{2011}]%
        {smith2011unsupervised}
\bibfield{author}{\bibinfo{person}{Tynan~S Smith}.}
  \bibinfo{year}{2011}\natexlab{}.
\newblock {\em \bibinfo{title}{Unsupervised discovery of human behavior and
  dialogue patterns in data from an online game}}.
\newblock \bibinfo{thesistype}{Ph.D. Dissertation}.
  \bibinfo{school}{Massachusetts Institute of Technology}.
\newblock


\bibitem[\protect\citeauthoryear{Soller and Stevens}{Soller and
  Stevens}{2007}]%
        {soller2007applications}
\bibfield{author}{\bibinfo{person}{Amy Soller} {and} \bibinfo{person}{Ron
  Stevens}.} \bibinfo{year}{2007}\natexlab{}.
\newblock \showarticletitle{Applications of stochastic analyses for
  collaborative learning and cognitive assessment}.
\newblock \bibinfo{journal}{{\em Advances in latent variable mixture models\/}}
  (\bibinfo{year}{2007}), \bibinfo{pages}{217--253}.
\newblock


\bibitem[\protect\citeauthoryear{Steinhaus}{Steinhaus}{1956}]%
        {steinhaus1956division}
\bibfield{author}{\bibinfo{person}{Hugo Steinhaus}.}
  \bibinfo{year}{1956}\natexlab{}.
\newblock \showarticletitle{Sur la division des corp materiels en parties}.
\newblock \bibinfo{journal}{{\em Bull. Acad. Polon. Sci\/}}
  \bibinfo{volume}{1}, \bibinfo{number}{804} (\bibinfo{year}{1956}),
  \bibinfo{pages}{801}.
\newblock


\bibitem[\protect\citeauthoryear{Sung and Hwang}{Sung and Hwang}{2013}]%
        {sung2013collaborative}
\bibfield{author}{\bibinfo{person}{Han-Yu Sung} {and} \bibinfo{person}{Gwo-Jen
  Hwang}.} \bibinfo{year}{2013}\natexlab{}.
\newblock \showarticletitle{A collaborative game-based learning approach to
  improving students' learning performance in science courses}.
\newblock \bibinfo{journal}{{\em Computers \& Education\/}}
  \bibinfo{volume}{63} (\bibinfo{year}{2013}), \bibinfo{pages}{43--51}.
\newblock


\bibitem[\protect\citeauthoryear{Te{\'o}filo and Reis}{Te{\'o}filo and
  Reis}{2013}]%
        {teofilo2013identifying}
\bibfield{author}{\bibinfo{person}{Lu{\'\i}s~Filipe Te{\'o}filo} {and}
  \bibinfo{person}{Lu{\'\i}s~Paulo Reis}.} \bibinfo{year}{2013}\natexlab{}.
\newblock \showarticletitle{Identifying Player$\backslash$'s Strategies in No
  Limit Texas Hold$\backslash$'em Poker through the Analysis of Individual
  Moves}.
\newblock \bibinfo{journal}{{\em arXiv preprint arXiv:1301.5943\/}}
  (\bibinfo{year}{2013}).
\newblock


\bibitem[\protect\citeauthoryear{Thurau and Bauckhage}{Thurau and
  Bauckhage}{2010}]%
        {thurau2010analyzing}
\bibfield{author}{\bibinfo{person}{Christian Thurau} {and}
  \bibinfo{person}{Christian Bauckhage}.} \bibinfo{year}{2010}\natexlab{}.
\newblock \showarticletitle{Analyzing the evolution of social groups in World
  of Warcraft{\textregistered}}. In \bibinfo{booktitle}{{\em Proceedings of the
  2010 IEEE Conference on Computational Intelligence and Games}}. IEEE,
  \bibinfo{pages}{170--177}.
\newblock


\bibitem[\protect\citeauthoryear{VanLehn}{VanLehn}{2008}]%
        {vanlehn2008intelligent}
\bibfield{author}{\bibinfo{person}{Kurt VanLehn}.}
  \bibinfo{year}{2008}\natexlab{}.
\newblock \showarticletitle{Intelligent tutoring systems for continuous,
  embedded assessment}.
\newblock \bibinfo{journal}{{\em The future of assessment: Shaping teaching and
  learning\/}} (\bibinfo{year}{2008}), \bibinfo{pages}{113--138}.
\newblock


\bibitem[\protect\citeauthoryear{von Davier}{von Davier}{2017}]%
        {JEM2017v54}
\bibfield{author}{\bibinfo{person}{Alina~A.(ED) von Davier}.}
  \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{Collaborative Educational Assessments [Special
  Issue]}. In \bibinfo{booktitle}{{\em Journal of Educational Measurement}}.
  Wiley Online Library, \bibinfo{pages}{v54, issue 1, pp 1--141}.
\newblock


\bibitem[\protect\citeauthoryear{von Davier}{von Davier}{2015}]%
        {vondavier2015a}
\bibfield{author}{\bibinfo{person}{A.~A. von Davier}.} \bibinfo{year}{July,
  2015}\natexlab{}.
\newblock \bibinfo{title}{Virtual and collaborative assessments: Examples,
  implications, and challenges for educational measurement}.
\newblock   (\bibinfo{year}{July, 2015}).
\newblock


\bibitem[\protect\citeauthoryear{von Davier and Halpin}{von Davier and
  Halpin}{2013}]%
        {davier2013collaborative}
\bibfield{author}{\bibinfo{person}{Alina~A von Davier} {and}
  \bibinfo{person}{Peter~F Halpin}.} \bibinfo{year}{2013}\natexlab{}.
\newblock \showarticletitle{Collaborative problem solving and the assessment of
  cognitive skills: Psychometric considerations}.
\newblock \bibinfo{journal}{{\em ETS Research Report Series\/}}
  \bibinfo{volume}{2013}, \bibinfo{number}{2} (\bibinfo{year}{2013}).
\newblock


\bibitem[\protect\citeauthoryear{von Davier, van~der Schaar, and Baraniuk}{von
  Davier et~al\mbox{.}}{July}]%
        {vondaviers2016}
\bibfield{author}{\bibinfo{person}{A.~A. von Davier}, \bibinfo{person}{M.
  van~der Schaar}, {and} \bibinfo{person}{R. Baraniuk}.} \bibinfo{year}{2016,
  July}\natexlab{}.
\newblock \showarticletitle{Workshop on Machine Learning for Education}.
\newblock \bibinfo{journal}{{\em International Conference of Machine Learning,
  New York, US\/}} (\bibinfo{year}{2016, July}).
\newblock


\bibitem[\protect\citeauthoryear{von Davier, Zhu, and Kyllonen}{von Davier
  et~al\mbox{.}}{2017}]%
        {vondavier2017}
\bibfield{author}{\bibinfo{person}{Alina~A. von Davier},
  \bibinfo{person}{Mengxiao Zhu}, {and} \bibinfo{person}{Patrick~C. Kyllonen}.}
  \bibinfo{year}{2017}\natexlab{}.
\newblock \bibinfo{booktitle}{{\em Innovative Assessment of Collaboration}}.
\newblock \bibinfo{publisher}{Springer}.
\newblock


\bibitem[\protect\citeauthoryear{Zhang, Hao, Li, and P.}{Zhang
  et~al\mbox{.}}{July}]%
        {zhang2015}
\bibfield{author}{\bibinfo{person}{M. Zhang}, \bibinfo{person}{J. Hao},
  \bibinfo{person}{C. Li}, {and} \bibinfo{person}{Deane P.}}
  \bibinfo{year}{2015, July}\natexlab{}.
\newblock \showarticletitle{Classification of writing styles using keystroke
  logs: a hierarchical vectorization approach}.
\newblock \bibinfo{journal}{{\em Paper presented at International Meeting of
  the Psychometric Society, Beijing, China\/}} (\bibinfo{year}{2015, July}).
\newblock


\end{thebibliography}
